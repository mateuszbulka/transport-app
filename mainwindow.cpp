#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMainWindow>
#include <iostream>
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtCore>
#include <QDebug>
#include "towar.h"
#include "miejsce.h"
#include "kierowca.h"
#include "pojazd.h"

using namespace std;

QString dane[49];
QString dane1[22];
QString dane2[34];
QString dane3[18];
double nowy_max=0;
bool czy_kierowca_moze_trasa0[8];
bool czy_kierowca_moze_trasa1[8];
bool czy_kierowca_moze_trasa2[8];
bool czy_kierowca_moze_trasa3[8];
bool czy_auto_moze_wraz_tow0[4];
bool czy_auto_moze_wraz_tow1[4];
bool czy_auto_moze_wraz_tow2[4];
bool czy_auto_moze_wraz_tow3[4];
bool czy_auto_moze_chlod_tow0[4];
bool czy_auto_moze_chlod_tow1[4];
bool czy_auto_moze_chlod_tow2[4];
bool czy_auto_moze_chlod_tow3[4];
bool czy_auto_moze_ilosc_tow0[4];
bool czy_auto_moze_ilosc_tow1[4];
bool czy_auto_moze_ilosc_tow2[4];
bool czy_auto_moze_ilosc_tow3[4];
bool czy_kierowca_moze_auto0[8];
bool czy_kierowca_moze_auto1[8];
bool czy_kierowca_moze_auto2[8];
bool czy_kierowca_moze_auto3[8];
double zmienione[50];
double IL; //ilosc stworzonego towaru
double kilometry=0; // odleglosc z magazynu A do B

//------------------FLAGI POJAZDÓW---------------//

bool temperatura; //auto podaje czy posiada chlodnie
bool wrazliwosc; // auto podaje czy posiada boxy dla koni
bool ladownosc; // auto sprawdza czy jest w stanie zabrac okreslona ilosc towaru

//-----------------------------------------------//


void wczytaj_kierowcy()
{

    QFile file("D:/kierowcy.txt");

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        for(int i=0 ; i<49 ; i++)
        {
            dane[i] = stream.readLine();
        }

        file.close();
    }
}

void wczytaj_trasy()
{
    QFile file("D:/trasy.txt");

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        for(int i=0 ; i<21 ; i++)
        {
            dane1[i] = stream.readLine();
        }

        file.close();
    }
}

void wczytaj_pojazdy()
{
    QFile file("D:/pojazdy.txt");

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        for(int i=0 ; i<33 ; i++)
        {
            dane2[i] = stream.readLine();
        }

        file.close();
    }
}

void wczytaj_towary()
{
    QFile file("D:/towary.txt");

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        for(int i=0 ; i<17 ; i++)
        {
            dane3[i] = stream.readLine();
        }

        file.close();
    }
}

void transformacja_kierowcy()
{
    int g=0;
    int k=0;
    for(int j=0 ; j<8 ; j++)
    {
         zmienione[j+g] = dane[k+4].toDouble();
         zmienione[j+1+g] = dane[k+5].toDouble();
         zmienione[j+2+g] = dane[k+6].toDouble();
         k=k+6;
         g=g+2;
    }
}

void transformacja_trasy()
{
    int g=0;
    int k=0;
    for(int j=0 ; j<4 ; j++)
    {

         zmienione[j+g] = dane1[k+4].toDouble();
         zmienione[j+1+g] = dane1[k+5].toDouble();
         k=k+5;
         g=g+1;
    }
}

void transformacja_pojazdy()
{
    int g=0;
    int k=0;
    for(int j=0 ; j<4 ; j++)
    {
         zmienione[j+g] = dane2[k+4].toDouble();
         zmienione[j+1+g] = dane2[k+5].toDouble();
         zmienione[j+2+g] = dane2[k+6].toDouble();
         zmienione[j+3+g] = dane2[k+7].toDouble();
         zmienione[j+4+g] = dane2[k+8].toDouble();
         k=k+8;
         g=g+4;
    }
}

void transformacja_towary()
{
    int g=0;
    int k=0;
    for(int j=0 ; j<4 ; j++)
    {

         zmienione[j+g] = dane3[k+3].toDouble();
         zmienione[j+1+g] = dane3[k+4].toDouble();
         k=k+4;
         g=g+1;
    }
}


//------------------------------------------------------//

//void MainWindow::on_IL_warz_editingFinished()
//{
//    IL1 = ui->IL_warz->text().toDouble();
//}




//------------------COMBOBOXY Z LOKACJAMI---------------//

void MainWindow::on_comboBox_2_activated(const QString &arg1)
{

}


//------------------PRZYCISK---------------//

void MainWindow::on_pushButton_clicked()
{

}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);


    wczytaj_kierowcy();
    transformacja_kierowcy();
    kierowca kier[8] = {kierowca(dane[2], dane[3], zmienione[0], zmienione[1], zmienione[2]), kierowca(dane[8], dane[9], zmienione[3], zmienione[4], zmienione[5]),
                       kierowca(dane[14], dane[15], zmienione[6], zmienione[7], zmienione[8]), kierowca(dane[20], dane[21], zmienione[9], zmienione[10], zmienione[11]),
                       kierowca(dane[26], dane[27], zmienione[12], zmienione[13], zmienione[14]), kierowca(dane[32], dane[33], zmienione[15], zmienione[16], zmienione[17]),
                       kierowca(dane[38], dane[39], zmienione[18], zmienione[19], zmienione[20]), kierowca(dane[44], dane[45], zmienione[21], zmienione[22], zmienione[23])};
    //kierowca kier[0] = (dane[2], dane[3], zmienione[0], zmienione[1], zmienione[2]);
    //kierowca kier[1] = (dane[8], dane[9], zmienione[3], zmienione[4], zmienione[5]);
    //kierowca kier[2] = (dane[14], dane[15], zmienione[6], zmienione[7], zmienione[8]);
    //kierowca kier[3] = (dane[20], dane[21], zmienione[9], zmienione[10], zmienione[11]);
    //kierowca kier[4] = (dane[26], dane[27], zmienione[12], zmienione[13], zmienione[14]);
    //kierowca kier[5] = (dane[32], dane[33], zmienione[15], zmienione[16], zmienione[17]);
    //kierowca kier[6] = (dane[38], dane[39], zmienione[18], zmienione[19], zmienione[20]);
    //kierowca kier[7] = (dane[44], dane[45], zmienione[21], zmienione[22], zmienione[23]);


    ui->label_15->setText(dane[2]), ui->label_16->setText(dane[3]), ui->label_17->setText(dane[4]), ui->label_18->setText(dane[5]), ui->label_19->setText(dane[6]);
    ui->label_20->setText(dane[8]), ui->label_21->setText(dane[9]), ui->label_22->setText(dane[10]), ui->label_23->setText(dane[11]), ui->label_24->setText(dane[12]);
    ui->label_25->setText(dane[14]), ui->label_26->setText(dane[15]), ui->label_27->setText(dane[16]), ui->label_28->setText(dane[17]), ui->label_29->setText(dane[18]);
    ui->label_30->setText(dane[20]), ui->label_31->setText(dane[21]), ui->label_32->setText(dane[22]), ui->label_33->setText(dane[23]), ui->label_34->setText(dane[24]);
    ui->label_35->setText(dane[26]), ui->label_36->setText(dane[27]), ui->label_37->setText(dane[28]), ui->label_38->setText(dane[29]), ui->label_39->setText(dane[30]);
    ui->label_40->setText(dane[32]), ui->label_41->setText(dane[33]), ui->label_42->setText(dane[34]), ui->label_43->setText(dane[35]), ui->label_44->setText(dane[36]);
    ui->label_45->setText(dane[38]), ui->label_46->setText(dane[39]), ui->label_47->setText(dane[40]), ui->label_48->setText(dane[41]), ui->label_49->setText(dane[42]);
    ui->label_50->setText(dane[44]), ui->label_51->setText(dane[45]), ui->label_52->setText(dane[46]), ui->label_53->setText(dane[47]), ui->label_54->setText(dane[48]);

    wczytaj_trasy();
    transformacja_trasy();
    miejsce trasa[4] = {miejsce(dane1[2], dane1[3], zmienione[0], zmienione[1]), miejsce(dane1[7], dane1[8], zmienione[2], zmienione[3]),
                       miejsce(dane1[12], dane1[13], zmienione[4], zmienione[5]), miejsce(dane1[17], dane1[18], zmienione[6], zmienione[7])};
    //miejsce trasa1(dane1[2], dane1[3], zmienione[0], zmienione[1]);
    //miejsce trasa2(dane1[7], dane1[8], zmienione[2], zmienione[3]);
    //miejsce trasa3(dane1[12], dane1[13], zmienione[4], zmienione[5]);
    //miejsce trasa4(dane1[17], dane1[18], zmienione[6], zmienione[7]);

    ui->comboBox_2->addItem("");
    ui->comboBox_2->addItem(trasa[0].daj_nazwe());
    ui->comboBox_2->addItem(trasa[1].daj_nazwe());
    ui->comboBox_2->addItem(trasa[2].daj_nazwe());
    ui->comboBox_2->addItem(trasa[3].daj_nazwe());

    wczytaj_pojazdy();
    transformacja_pojazdy();

    pojazd poj[4] = {pojazd(dane2[2], dane2[3], zmienione[0], zmienione[1], zmienione[2], zmienione[3], zmienione[4]), pojazd(dane2[10], dane2[11], zmienione[5], zmienione[6], zmienione[7], zmienione[8], zmienione[9]),
                    pojazd(dane2[18], dane2[19], zmienione[10], zmienione[11], zmienione[12], zmienione[13], zmienione[14]), pojazd(dane2[26], dane2[27], zmienione[15], zmienione[16], zmienione[17], zmienione[18], zmienione[19])};
    //pojazd poj1(dane2[2], dane2[3], zmienione[0], zmienione[1], zmienione[2], zmienione[3], zmienione[4]);
    //pojazd poj2(dane2[10], dane2[11], zmienione[5], zmienione[6], zmienione[7], zmienione[8], zmienione[9]);
    //pojazd poj3(dane2[18], dane2[19], zmienione[10], zmienione[11], zmienione[12], zmienione[13], zmienione[14]);
    //pojazd poj4(dane2[26], dane2[27], zmienione[15], zmienione[16], zmienione[17], zmienione[18], zmienione[19]);

    ui->label_1001->setText(dane2[2]), ui->label_1002->setText(dane2[3]), ui->label_1003->setText(dane2[4]), ui->label_1004->setText(dane2[5]), ui->label_1005->setText(dane2[6]), ui->label_1006->setText(dane2[7]), ui->label_1007->setText(dane2[8]);
    ui->label_1008->setText(dane2[10]), ui->label_1009->setText(dane2[11]), ui->label_1010->setText(dane2[12]), ui->label_1011->setText(dane2[13]), ui->label_1012->setText(dane2[14]), ui->label_1013->setText(dane2[15]), ui->label_1014->setText(dane2[16]);
    ui->label_1015->setText(dane2[18]), ui->label_1016->setText(dane2[19]), ui->label_1017->setText(dane2[20]), ui->label_1018->setText(dane2[21]), ui->label_1019->setText(dane2[22]), ui->label_1020->setText(dane2[23]), ui->label_1021->setText(dane2[24]);
    ui->label_1022->setText(dane2[26]), ui->label_1023->setText(dane2[27]), ui->label_1024->setText(dane2[28]), ui->label_1025->setText(dane2[29]), ui->label_1026->setText(dane2[30]), ui->label_1027->setText(dane2[31]), ui->label_1028->setText(dane2[32]);

    wczytaj_towary();
    transformacja_towary();

    towar tow[4] = {towar(dane3[2], zmienione[0], zmienione[1], 0), towar(dane3[6], zmienione[2], zmienione[3], 0),
                   towar(dane3[10], zmienione[4], zmienione[5], 0), towar(dane3[14], zmienione[6], zmienione[7], 0)};
    //towar tow1(dane3[2], zmienione[0], zmienione[1], 0);
    //towar tow2(dane3[6], zmienione[2], zmienione[3], 0);
    //towar tow3(dane3[10], zmienione[4], zmienione[5], 0);
    //towar tow4(dane3[14], zmienione[6], zmienione[7], 0);

    ui->label->setText(tow[0].daj_nazwe()), ui->label_2->setText(tow[1].daj_nazwe()), ui->label_3->setText(tow[2].daj_nazwe()), ui->label_4->setText(tow[3].daj_nazwe());

    //obliczenia
    
    //1. Dopasowanie trasy do kierowcy (wyznaczanie listy kierowcow ktorzy jezdza za granica i tych ktorzy jezdza tylko w kraju)

    for(int a=0; a<8; a++)
    {

           if(trasa[0].daj_kraj()==kier[a].daj_kraj() || kier[a].daj_kraj()==1)
           {
               czy_kierowca_moze_trasa0[a] = true;
           }
           if(trasa[1].daj_kraj()==kier[a].daj_kraj() || kier[a].daj_kraj()==1)
           {
               czy_kierowca_moze_trasa1[a] = true;
           }
           if(trasa[2].daj_kraj()==kier[a].daj_kraj() || kier[a].daj_kraj()==1)
           {
               czy_kierowca_moze_trasa2[a] = true;
           }
           if(trasa[3].daj_kraj()==kier[a].daj_kraj() || kier[a].daj_kraj()==1)
           {
               czy_kierowca_moze_trasa3[a] = true;
           }

    }
    
    //2. Dopasowanie wrazliwosci towaru do pojazdu (czy auto ma specjalne wymagania by przewiezc towar)
    
    for(int g=0; g<4; g++)
    {

           if(tow[0].daj_wrazliwosc()==poj[g].daj_wrazliwosc() || poj[g].daj_wrazliwosc()==1)
            {
                czy_auto_moze_wraz_tow0[g] = true;
            }
            if(tow[1].daj_wrazliwosc()==poj[g].daj_wrazliwosc() || poj[g].daj_wrazliwosc()==1)
            {
                czy_auto_moze_wraz_tow1[g] = true;
            }
            if(tow[2].daj_wrazliwosc()==poj[g].daj_wrazliwosc() || poj[g].daj_wrazliwosc()==1)
            {
                czy_auto_moze_wraz_tow2[g] = true;
            }
            if(tow[3].daj_wrazliwosc()==poj[g].daj_wrazliwosc() || poj[g].daj_wrazliwosc()==1)
            {
                czy_auto_moze_wraz_tow3[g] = true;
            }

    }

    //3. Dopasowanie chlodni towaru do pojazdu (czy auto ma chlodnie aby przewiezc towar)

    for(int g=0; g<4; g++)
    {

           if(tow[0].daj_chlodnie()==poj[g].daj_temp() || poj[g].daj_temp()==1)
            {
                czy_auto_moze_chlod_tow0[g] = true;
            }
            if(tow[1].daj_chlodnie()==poj[g].daj_temp() || poj[g].daj_temp()==1)
            {
                czy_auto_moze_chlod_tow1[g] = true;
            }
            if(tow[2].daj_chlodnie()==poj[g].daj_temp() || poj[g].daj_temp()==1)
            {
                czy_auto_moze_chlod_tow2[g] = true;
            }
            if(tow[3].daj_chlodnie()==poj[g].daj_temp() || poj[g].daj_temp()==1)
            {
                czy_auto_moze_chlod_tow3[g] = true;
            }

    }
    
    //4. Dopasowanie ilosci towaru do ladownosci (obliczanie czy towar zmiesci sie w danym samochodzie)

    for(int g=0; g<4; g++)
    {

            if(tow[0].daj_ilosc()<=poj[g].daj_ladownosc())
            {
                czy_auto_moze_ilosc_tow0[g] = true;
            }
            if(tow[1].daj_ilosc()<=poj[g].daj_ladownosc())
            {
                czy_auto_moze_ilosc_tow1[g] = true;
            }
            if(tow[2].daj_ilosc()<=poj[g].daj_ladownosc())
            {
               czy_auto_moze_ilosc_tow2[g] = true;
            }
            if(tow[3].daj_ilosc()<=poj[g].daj_ladownosc())
            {
                czy_auto_moze_ilosc_tow3[g] = true;
            }

    }

    //5. Dopasowanie kierowcow do pojazdow (obliczanie czy kierowca ma uprawnienia do prowadzenia pojazdu)

    for(int g=0; g<8; g++)
    {

            if(poj[0].daj_kategorie()==kier[g].daj_kategorie() || kier[g].daj_kategorie()=="C")
            {
                czy_kierowca_moze_auto0[g] = true;
            }
            if(poj[1].daj_kategorie()==kier[g].daj_kategorie() || kier[g].daj_kategorie()=="C")
            {
                czy_kierowca_moze_auto1[g] = true;
            }
            if(poj[2].daj_kategorie()==kier[g].daj_kategorie() || kier[g].daj_kategorie()=="C")
            {
                czy_kierowca_moze_auto2[g] = true;
            }
            if(poj[3].daj_kategorie()==kier[g].daj_kategorie() || kier[g].daj_kategorie()=="C")
            {
                czy_kierowca_moze_auto3[g] = true;
            }


    }

//pasujące pojazdy

for(int i=0; i<4; i++ )
{
    if(czy_auto_moze_chlod_tow0[i]==1 && czy_auto_moze_wraz_tow0[i]==1 && czy_auto_moze_ilosc_tow0[i]==1)
    {
        cout << "tak0" << i << endl;
    }

    if(czy_auto_moze_chlod_tow1[i]==1 && czy_auto_moze_wraz_tow1[i]==1 && czy_auto_moze_ilosc_tow1[i]==1)
    {
        cout << "tak1" << i << endl;
    }

    if(czy_auto_moze_chlod_tow2[i]==1 && czy_auto_moze_wraz_tow2[i]==1 && czy_auto_moze_ilosc_tow2[i]==1)
    {
        cout << "tak2" << i << endl;
    }

    if(czy_auto_moze_chlod_tow3[i]==1 && czy_auto_moze_wraz_tow3[i]==1 && czy_auto_moze_ilosc_tow3[i]==1)
    {
        cout << "tak3" << i << endl;
    }

}


}

MainWindow::~MainWindow()
{
    delete ui;
}

