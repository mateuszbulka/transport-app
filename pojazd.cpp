#include "pojazd.h"
#include <iostream>
#include <QDebug>

using namespace std;

void pojazd::pokaz()
{
    qDebug() << nazwa;
    qDebug() << kategoria;
    cout << ladownosc << endl;
    cout << spalanie << endl;
    cout << sred_pred << endl;
    cout << temp << endl;
    cout << wrazliwosc << endl;
}

double pojazd::daj_predkosc()
{
    return sred_pred;
}

double pojazd::daj_ladownosc()
{
    return ladownosc;
}

double pojazd::daj_wrazliwosc()
{
    return wrazliwosc;
}

double pojazd::daj_temp()
{
    return temp;
}

QString pojazd::daj_kategorie()
{
    return kategoria;
}

QString pojazd::daj_nazwe()
{
    return nazwa;
}
