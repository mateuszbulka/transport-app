#include "kierowca.h"
#include "mainwindow.h"
#include <iostream>
#include <QString>
#include <QDebug>


using namespace std;

void kierowca::pokaz()
{
    qDebug() << imie;
    qDebug() << kategoria;
    cout << stawka << endl;
    cout << doswiadczenie << endl;
    cout << kraj << endl;
}

double kierowca::daj_kraj()
{
    return kraj;
}

double kierowca::daj_doswiadczenie()
{
    return doswiadczenie;
}

double kierowca::daj_stawke()
{
    return stawka;
}

QString kierowca::daj_kategorie()
{
    return kategoria;
}

QString kierowca::daj_imie()
{
    return imie;
}
