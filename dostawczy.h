#ifndef DOSTAWCZY_H
#define DOSTAWCZY_H
#include "pojazd.h"
#include "mainwindow.h"

class dostawczy :public pojazd
{
public:
    dostawczy(char kat, double paka, bool spec, bool kon, int spal, int sred )
    {
        kategoria = kat;
        bagaznik = paka;
        temp = spec;
        boxy = kon;
        spalanie = spal;
        sred_pred = sred;
    }
    friend void sprawdz(dostawczy przyk);
    friend void sprawdz_ladownosc(dostawczy przyk);

};

#endif // DOSTAWCZY_H
