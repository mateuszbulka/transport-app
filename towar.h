#pragma once
#include <iostream>
#include <QString>
#include <mainwindow.h>
#include <ui_mainwindow.h>


#ifndef TOWAR_H
#define TOWAR_H

using namespace std;

class towar
{
protected:


public:
    QString nazwa;
    double chlodnia;
    double wrazliwosc;
    double ilosc;

    towar(QString naz, double chlod, double wraz, double il)
    {
       nazwa = naz;
       chlodnia = chlod;
       wrazliwosc = wraz;
       ilosc = il;
    }
    void zmien(double x);
    void pokaz();
    QString daj_nazwe();
    double daj_ilosc();
    double daj_wrazliwosc();
    double daj_chlodnie();

};

#endif // TOWAR_H
