#ifndef MIEJSCE_H
#define MIEJSCE_H
#include <QString>

class miejsce
{
protected:
    QString nazwa;
    QString punkta;
    QString punktb;
    double odleglosc;
    double kraj;
public:
    miejsce(QString pkta,QString pktb, double odl, double kr)
    {
        nazwa = pkta+" <-> "+pktb;
        odleglosc = odl;
        kraj = kr;
    }
    void pokaz();
    double daj_kraj();
    double daj_odleglosc();
    QString daj_nazwe();

};

#endif // MIEJSCE_H
