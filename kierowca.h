#ifndef KIEROWCA_H
#define KIEROWCA_H
#include <QString>
#include "mainwindow.h"

class kierowca
{
protected:
    QString imie;
    QString kategoria;
    double stawka;
    int doswiadczenie;
    int kraj;
public:
    kierowca(QString im,QString kat, int staw,int dosw, int kr)
    {
        imie = im;
        kategoria = kat;
        stawka = staw;
        doswiadczenie = dosw;
        kraj = kr;
    }
    void pokaz();
    double daj_kraj();
    double daj_doswiadczenie();
    double daj_stawke();
    QString daj_kategorie();
    QString daj_imie();

};

#endif // KIEROWCA_H
