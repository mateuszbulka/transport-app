#ifndef AUTO_H
#define AUTO_H
#include "towar.h"
#include <QString>

class pojazd
{
protected:
    QString nazwa;
    QString kategoria;
    double ladownosc;
    double spalanie;
    double sred_pred;
    double temp;
    double wrazliwosc;
public:
    pojazd(QString naz, QString kat, double lad, double spal, double sred, double tp, double wraz)
    {
        nazwa = naz;
        kategoria = kat;
        ladownosc = lad;
        spalanie = spal;
        sred_pred = sred;
        temp = tp;
        wrazliwosc = wraz;
    }
    void pokaz();
    double daj_predkosc();
    double daj_ladownosc();
    double daj_wrazliwosc();
    double daj_temp();
    QString daj_kategorie();
    QString daj_nazwe();
};

#endif // AUTO_H
